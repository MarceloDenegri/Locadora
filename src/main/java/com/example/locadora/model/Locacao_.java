package com.example.locadora.model;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Locacao.class)
public abstract class Locacao_ {

	public static volatile SingularAttribute<Locacao, LocalDate> dtretirda;
	public static volatile SingularAttribute<Locacao, Float> valor_total;
	public static volatile SingularAttribute<Locacao, Veiculo> veiculo;
	public static volatile SingularAttribute<Locacao, Pessoa> pessoa;
	public static volatile SingularAttribute<Locacao, Long> id;
	public static volatile SingularAttribute<Locacao, Float> saldo;
	public static volatile SingularAttribute<Locacao, Pagamento> pagamento;
	public static volatile SingularAttribute<Locacao, Forma> forma;
	public static volatile SingularAttribute<Locacao, LocalDate> dtentrega;

}

