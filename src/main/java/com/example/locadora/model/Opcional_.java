package com.example.locadora.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Opcional.class)
public abstract class Opcional_ {

	public static volatile SingularAttribute<Opcional, Integer> qtd;
	public static volatile SingularAttribute<Opcional, String> nome;
	public static volatile SingularAttribute<Opcional, Long> id;

}

