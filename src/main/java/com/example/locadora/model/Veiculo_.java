package com.example.locadora.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Veiculo.class)
public abstract class Veiculo_ {

	public static volatile SingularAttribute<Veiculo, Boolean> cambio;
	public static volatile SingularAttribute<Veiculo, Motor> motor;
	public static volatile SingularAttribute<Veiculo, Integer> portas;
	public static volatile SingularAttribute<Veiculo, Combustivel> combustivel;
	public static volatile SingularAttribute<Veiculo, Categoria> categoria;
	public static volatile SingularAttribute<Veiculo, Float> valor;
	public static volatile SingularAttribute<Veiculo, Modelo> modelo;
	public static volatile SingularAttribute<Veiculo, Boolean> ar;
	public static volatile SingularAttribute<Veiculo, Marca> marca;
	public static volatile SingularAttribute<Veiculo, Opcional> opcional;
	public static volatile SingularAttribute<Veiculo, Boolean> trava;
	public static volatile SingularAttribute<Veiculo, Boolean> disponivel;
	public static volatile SingularAttribute<Veiculo, Long> id;
	public static volatile SingularAttribute<Veiculo, Boolean> vidro;
	public static volatile SingularAttribute<Veiculo, String> placa;

}

