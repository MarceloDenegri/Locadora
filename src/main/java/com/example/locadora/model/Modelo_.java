package com.example.locadora.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Modelo.class)
public abstract class Modelo_ {

	public static volatile SingularAttribute<Modelo, Marca> marca;
	public static volatile SingularAttribute<Modelo, String> nome;
	public static volatile SingularAttribute<Modelo, Long> id;

}

