package com.example.locadora.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Motor.class)
public abstract class Motor_ {

	public static volatile SingularAttribute<Motor, String> potencia;
	public static volatile SingularAttribute<Motor, Long> id;

}

