package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Opcional;
import com.example.locadora.repository.OpcionalRepository;
import com.example.locadora.service.OpcionalService;

@RestController
@RequestMapping("/opcional")
public class OpcionalResources {
	@Autowired
	private OpcionalRepository opcionalRepository;
	
	@Autowired
	private OpcionalService opcionalService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_OPCIONAL') and #oauth2.hasScope('read')")
	public List<Opcional> listar(){
		return opcionalRepository.findAll();
		
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_OPCIONAL') and #oauth2.hasScope('write')")
	public ResponseEntity<Opcional> criar(@Valid @RequestBody Opcional opcional, HttpServletResponse response) {
		Opcional opcionalSalvo = opcionalRepository.save(opcional);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, opcionalSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(opcionalSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_OPCIONAL') and #oauth2.hasScope('read')")
	public ResponseEntity<Opcional> buscarPeloId(@PathVariable Long id) {
		 Opcional opcional = opcionalRepository.findOne(id);
		 return opcional != null ? ResponseEntity.ok(opcional) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_OPCIONAL') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				opcionalRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_OPCIONAL') and #oauth2.hasScope('write')")
	public ResponseEntity<Opcional> atualizar(@PathVariable Long id, @Valid @RequestBody Opcional opcional) {
		Opcional opcionalSalvo = opcionalService.atualizar(id, opcional);
		return ResponseEntity.ok(opcionalSalvo);	
		
	}

}
