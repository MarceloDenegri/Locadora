package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Modelo;
import com.example.locadora.repository.ModeloRepository;
import com.example.locadora.service.ModeloService;

@RestController
@RequestMapping("/modelo")
public class ModeloResources {

	@Autowired
	private ModeloRepository modeloRepository;
	
	@Autowired
	private ModeloService modeloService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MODELO') and #oauth2.hasScope('read')")
	public List<Modelo> listar(){
		return modeloRepository.findAll();
		
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MODELO') and #oauth2.hasScope('write')")
	public ResponseEntity<Modelo> criar(@Valid @RequestBody Modelo modelo, HttpServletResponse response) {
		Modelo modeloSalvo = modeloRepository.save(modelo);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, modeloSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(modeloSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MODELO') and #oauth2.hasScope('read')")
	public ResponseEntity<Modelo> buscarPeloId(@PathVariable Long id) {
		 Modelo modelo = modeloRepository.findOne(id);
		 return modelo != null ? ResponseEntity.ok(modelo) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_MODELO') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				modeloRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MODELO') and #oauth2.hasScope('write')")
	public ResponseEntity<Modelo> atualizar(@PathVariable Long id, @Valid @RequestBody Modelo modelo) {
		Modelo modeloSalvo = modeloService.atualizar(id, modelo);
		return ResponseEntity.ok(modeloSalvo);	
		
	}

}
