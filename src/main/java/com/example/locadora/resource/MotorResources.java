package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Motor;
import com.example.locadora.repository.MotorRepository;
import com.example.locadora.service.MotorService;

@RestController
@RequestMapping("/motor")
public class MotorResources {
	
	@Autowired
	private MotorRepository motorRepository;
	
	@Autowired
	private MotorService motorService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MOTOR') and #oauth2.hasScope('read')")
	public List<Motor> listar(){
		return motorRepository.findAll();
		
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MOTOR') and #oauth2.hasScope('write')")
	public ResponseEntity<Motor> criar(@Valid @RequestBody Motor motor, HttpServletResponse response) {
		Motor motorSalvo = motorRepository.save(motor);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, motorSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(motorSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MOTOR') and #oauth2.hasScope('read')")
	public ResponseEntity<Motor> buscarPeloId(@PathVariable Long id) {
		 Motor motor = motorRepository.findOne(id);
		 return motor != null ? ResponseEntity.ok(motor) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_MOTOR') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				motorRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MOTOR') and #oauth2.hasScope('write')")
	public ResponseEntity<Motor> atualizar(@PathVariable Long id, @Valid @RequestBody Motor motor) {
		Motor motorSalvo = motorService.atualizar(id, motor);
		return ResponseEntity.ok(motorSalvo);	
		
	}

}
