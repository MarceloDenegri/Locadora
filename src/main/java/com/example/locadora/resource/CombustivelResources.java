package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Combustivel;
import com.example.locadora.repository.CombustivelRepository;
import com.example.locadora.service.CombustivelService;

@RestController
@RequestMapping("/combustivel")
public class CombustivelResources {
	
	@Autowired
	private CombustivelRepository combustivelRepository;
	
	@Autowired
	private CombustivelService combustivelService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_COMBUSTIVEL') and #oauth2.hasScope('read')")
	public List<Combustivel> listar(){
		return combustivelRepository.findAll();
		
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_COMBUSTIVEL') and #oauth2.hasScope('write')")
	public ResponseEntity<Combustivel> criar(@Valid @RequestBody Combustivel combustivel, HttpServletResponse response) {
		Combustivel combustivelSalvo = combustivelRepository.save(combustivel);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, combustivelSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(combustivelSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_COMBUSTIVEL') and #oauth2.hasScope('read')")
	public ResponseEntity<Combustivel> buscarPeloId(@PathVariable Long id) {
		 Combustivel combustivel = combustivelRepository.findOne(id);
		 return combustivel != null ? ResponseEntity.ok(combustivel) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_COMBUSTIVEL') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				combustivelRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_COMBUSTIVEL') and #oauth2.hasScope('write')")
	public ResponseEntity<Combustivel> atualizar(@PathVariable Long id, @Valid @RequestBody Combustivel combustivel) {
		Combustivel combustivelSalvo = combustivelService.atualizar(id, combustivel);
		return ResponseEntity.ok(combustivelSalvo);	
		
	}

}
