package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Forma;
import com.example.locadora.repository.FormaRepository;
import com.example.locadora.service.FormaService;

@RestController
@RequestMapping("/forma")
public class FormaResources {
	@Autowired
	private FormaRepository formaRepository;
	
	@Autowired
	private FormaService formaService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_FORMA') and #oauth2.hasScope('read')")
	public List<Forma> listar() {
		return formaRepository.findAll();
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_FORMA') and #oauth2.hasScope('write')")
	public ResponseEntity<Forma> criar(@Valid @RequestBody Forma forma, HttpServletResponse response) {
		Forma formaSalvo = formaRepository.save(forma);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, formaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(formaSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_FORMA') and #oauth2.hasScope('read')")
	public ResponseEntity<Forma> buscarPeloId(@PathVariable Long id) {
		 Forma forma = formaRepository.findOne(id);
		 return forma != null ? ResponseEntity.ok(forma) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_FORMA') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				formaRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_FORMA') and #oauth2.hasScope('write')")
	public ResponseEntity<Forma> atualizar(@PathVariable Long id, @Valid @RequestBody Forma forma) {
		Forma formaSalvo = formaService.atualizar(id, forma);
		return ResponseEntity.ok(formaSalvo);	
		
	}

}
