package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Cidade;
import com.example.locadora.repository.CidadeRepository;
import com.example.locadora.service.CidadeService;

@RestController
@RequestMapping("/cidade")
public class CidadeResources {
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private CidadeService cidadeService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CIDADE') and #oauth2.hasScope('read')")
	public List<Cidade> listar() {
		return cidadeRepository.findAll();
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CIDADE') and #oauth2.hasScope('write')")
	public ResponseEntity<Cidade> criar(@Valid @RequestBody Cidade cidade, HttpServletResponse response) {
		Cidade cidadeSalvo = cidadeRepository.save(cidade);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, cidadeSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(cidadeSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_CIDADE') and #oauth2.hasScope('read')")
	public ResponseEntity<Cidade> buscarPeloId(@PathVariable Long id) {
		 Cidade cidade = cidadeRepository.findOne(id);
		 return cidade != null ? ResponseEntity.ok(cidade) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_CIDADE') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				cidadeRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_CIDADE') and #oauth2.hasScope('write')")
	public ResponseEntity<Cidade> atualizar(@PathVariable Long id, @Valid @RequestBody Cidade cidade) {
		Cidade cidadeSalvo = cidadeService.atualizar(id, cidade);
		return ResponseEntity.ok(cidadeSalvo);	
		
	}

}
