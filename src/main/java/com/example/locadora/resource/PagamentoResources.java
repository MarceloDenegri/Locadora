package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Pagamento;
import com.example.locadora.repository.PagamentoRepository;
import com.example.locadora.service.PagamentoService;

@RestController
@RequestMapping("/pagamento")
public class PagamentoResources {
	
	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private PagamentoService pagamentoService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PAGAMENTO') and #oauth2.hasScope('read')")
	public List<Pagamento> listar() {
		return pagamentoRepository.findAll();
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PAGAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Pagamento> criar(@Valid @RequestBody Pagamento pagamento, HttpServletResponse response) {
		Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pagamentoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(pagamentoSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PAGAMENTO') and #oauth2.hasScope('read')")
	public ResponseEntity<Pagamento> buscarPeloId(@PathVariable Long id) {
		 Pagamento pagamento = pagamentoRepository.findOne(id);
		 return pagamento != null ? ResponseEntity.ok(pagamento) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_PAGAMENTO') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				pagamentoRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PAGAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Pagamento> atualizar(@PathVariable Long id, @Valid @RequestBody Pagamento pagamento) {
		Pagamento pagamentoSalvo = pagamentoService.atualizar(id, pagamento);
		return ResponseEntity.ok(pagamentoSalvo);	
		
	}

}
