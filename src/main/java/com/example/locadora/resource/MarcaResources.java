package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Marca;
import com.example.locadora.repository.MarcaRepository;
import com.example.locadora.service.MarcaService;

@RestController
@RequestMapping("/marca")
public class MarcaResources {
	
	@Autowired
	private MarcaRepository marcaRepository;
	
	@Autowired
	private MarcaService marcaService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MARCA') and #oauth2.hasScope('read')")
	public List<Marca> listar(){
		return marcaRepository.findAll();
		
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MARCA') and #oauth2.hasScope('write')")
	public ResponseEntity<Marca> criar(@Valid @RequestBody Marca marca, HttpServletResponse response) {
		Marca marcaSalvo = marcaRepository.save(marca);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, marcaSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(marcaSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_MARCA') and #oauth2.hasScope('read')")
	public ResponseEntity<Marca> buscarPeloId(@PathVariable Long id) {
		 Marca marca = marcaRepository.findOne(id);
		 return marca != null ? ResponseEntity.ok(marca) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_MARCA') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				marcaRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_MARCA') and #oauth2.hasScope('write')")
	public ResponseEntity<Marca> atualizar(@PathVariable Long id, @Valid @RequestBody Marca marca) {
		Marca marcaSalvo = marcaService.atualizar(id, marca);
		return ResponseEntity.ok(marcaSalvo);	
		
	}

}
