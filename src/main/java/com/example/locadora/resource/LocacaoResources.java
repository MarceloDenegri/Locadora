package com.example.locadora.resource;

import java.util.List;
import java.util.Arrays;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.execeptionhandler.LocadoraExceptionHandler.Erro;
import com.example.locadora.model.Locacao;
import com.example.locadora.repository.LocacaoRepository;
import com.example.locadora.repository.filter.LocacaoFilter;
import com.example.locadora.service.LocacaoService;
import com.example.locadora.service.exception.VeiculoInexistenteOuLocadoException;

@RestController
@RequestMapping("/locacao")
public class LocacaoResources {
	@Autowired
	private LocacaoRepository locacaoRepository;
	
	@Autowired
	private LocacaoService locacaoService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@Autowired
	private MessageSource messageSource;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_LOCACAO') and #oauth2.hasScope('read')")
	public Page<Locacao> pesquisar(LocacaoFilter locacaoFilter, Pageable pageable) {
		return locacaoRepository.filtrar(locacaoFilter, pageable);
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_LOCACAO') and #oauth2.hasScope('write')")
	public ResponseEntity<Locacao> criar(@Valid @RequestBody Locacao locacao, HttpServletResponse response) {
		Locacao locacaoSalvo = locacaoService.salvar(locacao);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, locacaoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(locacaoSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_LOCACAO') and #oauth2.hasScope('read')")
	public ResponseEntity<Locacao> buscarPeloId(@PathVariable Long id) {
		 Locacao locacao = locacaoRepository.findOne(id);
		 return locacao != null ? ResponseEntity.ok(locacao) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_LOCACAO') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				locacaoRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_LOCACAO') and #oauth2.hasScope('write')")
	public ResponseEntity<Locacao> atualizar(@PathVariable Long id, @Valid @RequestBody Locacao locacao) {
		Locacao locacaoSalvo = locacaoService.atualizar(id, locacao);
		return ResponseEntity.ok(locacaoSalvo);	
		
	}
	
	@ExceptionHandler({ VeiculoInexistenteOuLocadoException.class })
	public ResponseEntity<Object> handleVeiculoInexistenteOuLocadoException(VeiculoInexistenteOuLocadoException ex) {
		String mensagemUsuario = messageSource.getMessage("veiculo.inexistente-ou-locado", null, LocaleContextHolder.getLocale());
		String mensagemDesenvolvedor = ex.toString();
		List<Erro> erros = Arrays.asList(new Erro(mensagemUsuario, mensagemDesenvolvedor));
		return ResponseEntity.badRequest().body(erros);
	}

}
