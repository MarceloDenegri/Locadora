package com.example.locadora.resource;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Veiculo;
import com.example.locadora.repository.VeiculoRepository;
import com.example.locadora.repository.filter.VeiculoFilter;
import com.example.locadora.service.VeiculoService;

@RestController
@RequestMapping("/veiculo")
public class VeiculoResources {
	
	@Autowired
	private VeiculoRepository veiculoRepository;
	
	@Autowired
	private VeiculoService veiculoService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	//retornar paginas de uma pesquisa Paginacao
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VEICULO') and #oauth2.hasScope('read')")
	public Page<Veiculo> pesquisar(VeiculoFilter veiculoFilter, Pageable pageable) {
		return veiculoRepository.filtrar(veiculoFilter, pageable);
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_VEICULO') and #oauth2.hasScope('write')")
	public ResponseEntity<Veiculo> criar(@Valid @RequestBody Veiculo veiculo, HttpServletResponse response) {
		Veiculo veiculoSalvo = veiculoRepository.save(veiculo);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, veiculoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(veiculoSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VEICULO') and #oauth2.hasScope('read')")
	public ResponseEntity<Veiculo> buscarPeloId(@PathVariable Long id) {
		 Veiculo veiculo = veiculoRepository.findOne(id);
		 return veiculo != null ? ResponseEntity.ok(veiculo) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_VEICULO') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				veiculoRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_VEICULO') and #oauth2.hasScope('write')")
	public ResponseEntity<Veiculo> atualizar(@PathVariable Long id, @Valid @RequestBody Veiculo veiculo) {
		Veiculo veiculoSalvo = veiculoService.atualizar(id, veiculo);
		return ResponseEntity.ok(veiculoSalvo);	
		
	}
	
	@PutMapping("/{id}/disponivel")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void atualizarPropriedadeDisponivel(@PathVariable Long id, @RequestBody Boolean disponivel) {
		veiculoService.atualizarPropriedadeDisponivel(id, disponivel);
	}

}



