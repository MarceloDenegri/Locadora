package com.example.locadora.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.example.locadora.event.RecursoCriadoEvent;
import com.example.locadora.model.Estado;
import com.example.locadora.repository.EstadoRepository;
import com.example.locadora.service.EstadoService;

@RestController
@RequestMapping("/estado")
public class EstadoResource {
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private EstadoService estadoService;
		
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ESTADO') and #oauth2.hasScope('read')")
	public List<Estado> listar() {
		return estadoRepository.findAll();
		
	}
		
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ESTADO') and #oauth2.hasScope('write')")
	public ResponseEntity<Estado> criar(@Valid @RequestBody Estado estado, HttpServletResponse response) {
		Estado estadoSalvo = estadoRepository.save(estado);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, estadoSalvo.getId()));
		return ResponseEntity.status(HttpStatus.CREATED).body(estadoSalvo);
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_ESTADO') and #oauth2.hasScope('read')")
	public ResponseEntity<Estado> buscarPeloId(@PathVariable Long id) {
		 Estado estado = estadoRepository.findOne(id);
		 return estado != null ? ResponseEntity.ok(estado) : ResponseEntity.notFound().build();
		
	}
		 
		 @DeleteMapping("/{id}")
		 @PreAuthorize("hasAuthority('ROLE_REMOVER_ESTADO') and #oauth2.hasScope('write')")
			@ResponseStatus(HttpStatus.NO_CONTENT)
			public void remover(@PathVariable Long id) {
				estadoRepository.delete(id);
		 }
				
	@PutMapping("/{id}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_ESTADO') and #oauth2.hasScope('write')")
	public ResponseEntity<Estado> atualizar(@PathVariable Long id, @Valid @RequestBody Estado estado) {
		Estado estadoSalvo = estadoService.atualizar(id, estado);
		return ResponseEntity.ok(estadoSalvo);		
		 
	}
	
}