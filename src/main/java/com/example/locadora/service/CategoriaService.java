package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Categoria;
import com.example.locadora.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository categoriaRepository;

	public Categoria atualizar(Long id, Categoria categoria) {
		Categoria categoriaSalvo = categoriaRepository.findOne(id);
		if (categoriaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(categoria, categoriaSalvo, "id");
		return categoriaRepository.save(categoriaSalvo);
	}

}
