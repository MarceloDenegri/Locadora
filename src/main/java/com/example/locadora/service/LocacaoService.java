package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Locacao;
import com.example.locadora.model.Veiculo;
import com.example.locadora.repository.LocacaoRepository;
import com.example.locadora.repository.VeiculoRepository;
import com.example.locadora.service.exception.VeiculoInexistenteOuLocadoException;

@Service
public class LocacaoService {
	@Autowired
	private LocacaoRepository locacaoRepository;
	
	@Autowired
	private VeiculoRepository veiculoRepository;

	public Locacao atualizar(Long id, Locacao locacao) {
		Locacao locacaoSalvo = locacaoRepository.findOne(id);
		if (locacaoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(locacao, locacaoSalvo, "id");
		return locacaoRepository.save(locacaoSalvo);
	}

	public Locacao salvar(Locacao locacao) {
		Veiculo veiculo = veiculoRepository.findOne(locacao.getVeiculo().getId());
		if (veiculo == null || veiculo.isLocado()) {
			throw new VeiculoInexistenteOuLocadoException();
		}
		
		return locacaoRepository.save(locacao);
	}
	
}
