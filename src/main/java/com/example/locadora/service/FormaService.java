package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Forma;
import com.example.locadora.repository.FormaRepository;

@Service
public class FormaService {
	@Autowired
	private FormaRepository formaRepository;

	public Forma atualizar(Long id, Forma forma) {
		Forma formaSalvo = formaRepository.findOne(id);
		if (formaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(forma, formaSalvo, "id");
		return formaRepository.save(formaSalvo);
	}

}
