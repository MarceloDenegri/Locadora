package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Combustivel;
import com.example.locadora.repository.CombustivelRepository;

@Service
public class CombustivelService {
	@Autowired
	private CombustivelRepository combustivelRepository;

	public Combustivel atualizar(Long id, Combustivel combustivel) {
		Combustivel combustivelSalvo = combustivelRepository.findOne(id);
		if (combustivelSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(combustivel, combustivelSalvo, "id");
		return combustivelRepository.save(combustivelSalvo);
	}

}
