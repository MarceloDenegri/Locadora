package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Modelo;
import com.example.locadora.repository.ModeloRepository;

@Service
public class ModeloService {
	@Autowired
	private ModeloRepository modeloRepository;

	public Modelo atualizar(Long id, Modelo modelo) {
		Modelo modeloSalvo = modeloRepository.findOne(id);
		if (modeloSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(modelo, modeloSalvo, "id");
		return modeloRepository.save(modeloSalvo);
	}

}
