package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Motor;
import com.example.locadora.repository.MotorRepository;

@Service
public class MotorService {

	@Autowired
	private MotorRepository motorRepository;

	public Motor atualizar(Long id, Motor motor) {
		Motor motorSalvo = motorRepository.findOne(id);
		if (motorSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(motor, motorSalvo, "id");
		return motorRepository.save(motorSalvo);
	}

}
