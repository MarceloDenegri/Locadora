package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Marca;
import com.example.locadora.repository.MarcaRepository;


@Service
public class MarcaService {
	@Autowired
	private MarcaRepository marcaRepository;

	public Marca atualizar(Long id, Marca marca) {
		Marca marcaSalvo = marcaRepository.findOne(id);
		if (marcaSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(marca, marcaSalvo, "id");
		return marcaRepository.save(marcaSalvo);
	}

}
