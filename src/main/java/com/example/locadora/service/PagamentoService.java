package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Pagamento;
import com.example.locadora.repository.PagamentoRepository;

@Service
public class PagamentoService {
	@Autowired
	private PagamentoRepository pagamentoRepository;

	public Pagamento atualizar(Long id, Pagamento pagamento) {
		Pagamento pagamentoSalvo = pagamentoRepository.findOne(id);
		if (pagamentoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(pagamento, pagamentoSalvo, "id");
		return pagamentoRepository.save(pagamentoSalvo);
	}

}