package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Opcional;
import com.example.locadora.repository.OpcionalRepository;


@Service
public class OpcionalService {
	@Autowired
	private OpcionalRepository opcionalRepository;

	public Opcional atualizar(Long id, Opcional opcional) {
		Opcional opcionalSalvo = opcionalRepository.findOne(id);
		if (opcionalSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(opcional, opcionalSalvo, "id");
		return opcionalRepository.save(opcionalSalvo);
	}

}
