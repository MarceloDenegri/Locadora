package com.example.locadora.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.example.locadora.model.Veiculo;
import com.example.locadora.repository.VeiculoRepository;

@Service
public class VeiculoService {

	@Autowired
	private VeiculoRepository veiculoRepository;

	public Veiculo atualizar(Long id, Veiculo veiculo) {
		Veiculo veiculoSalvo = veiculoRepository.findOne(id);
		if (veiculoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		
		BeanUtils.copyProperties(veiculo, veiculoSalvo, "id");
		return veiculoRepository.save(veiculoSalvo);
	}

	public void atualizarPropriedadeDisponivel(Long id, Boolean disponivel) {
		Veiculo veiculoSalvo = buscarVeiculoPeloId(id);
		veiculoSalvo.setDisponivel(disponivel);
		veiculoRepository.save(veiculoSalvo);
		
	}
	
	public Veiculo buscarVeiculoPeloId(Long id) {
		Veiculo veiculoSalvo = veiculoRepository.findOne(id);
		if (veiculoSalvo == null) {
			throw new EmptyResultDataAccessException(1);
		}
		return veiculoSalvo;
	}

}
