package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Estado;

public interface EstadoRepository extends JpaRepository<Estado, Long>{

}
