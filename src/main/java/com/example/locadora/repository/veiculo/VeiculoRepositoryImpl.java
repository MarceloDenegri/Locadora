package com.example.locadora.repository.veiculo;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.olap4j.impl.ArrayMap;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.locadora.model.Categoria_;
import com.example.locadora.model.Combustivel_;
import com.example.locadora.model.Marca_;
import com.example.locadora.model.Modelo_;
import com.example.locadora.model.Motor_;
import com.example.locadora.model.Veiculo;
import com.example.locadora.model.Veiculo_;
import com.example.locadora.repository.filter.VeiculoFilter;

public class VeiculoRepositoryImpl implements VeiculoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Veiculo> filtrar(VeiculoFilter veiculoFilter, Pageable pageable){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Veiculo> criteria = builder.createQuery(Veiculo.class);
		//root é usado para pegar os atributos da classe
		Root<Veiculo> root = criteria.from(Veiculo.class);
		//restriçoes filtros
		Predicate[] predicates = criarRestricoes(veiculoFilter, builder, root);
		criteria.where(predicates);
		
		
		TypedQuery<Veiculo> query = manager.createQuery(criteria);
		//Adicionar na query a qtd total de resultados e onde ele vai começar
		adicionarRestircoesDePaginacao(query, pageable);
		
		//É passado o conteudo, o pageable o objeto e a quatidade de elementos para o filtro
		return new PageImpl<>(query.getResultList(), pageable, total(veiculoFilter)) ;
	}

	

	



	private Predicate[] criarRestricoes(VeiculoFilter veiculoFilter, CriteriaBuilder builder,
			Root<Veiculo> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(veiculoFilter.getCombustivel())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Veiculo_.combustivel).get(Combustivel_.nome)),
				"%" + veiculoFilter.getCombustivel().toLowerCase()+ "%"));
		}
		
		
		if (!StringUtils.isEmpty(veiculoFilter.getMarca())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Veiculo_.modelo).get(Modelo_.marca).get(Marca_.nome)),
				"%" + veiculoFilter.getMarca().toLowerCase()+ "%"));
		}
		
		if (!StringUtils.isEmpty(veiculoFilter.getMotor())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Veiculo_.motor).get(Motor_.potencia)),
				"%" + veiculoFilter.getMotor().toLowerCase()+ "%"));
		}
		
		if (!StringUtils.isEmpty(veiculoFilter.getModelo())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Veiculo_.modelo).get(Modelo_.nome)),
				"%" + veiculoFilter.getModelo().toLowerCase()+ "%"));
		}
		
		if (!StringUtils.isEmpty(veiculoFilter.getCategoria())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Veiculo_.categoria).get(Categoria_.nome)),
				"%" + veiculoFilter.getCategoria().toLowerCase()+ "%"));
		}
		
			
		
		
		return predicates.toArray(new Predicate[predicates.size()]);
		}
	
	private void adicionarRestircoesDePaginacao(TypedQuery<Veiculo> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalDeRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalDeRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalDeRegistrosPorPagina);
		
	}
	
	private Long total(VeiculoFilter veiculoFilter) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		//A consulta está sendo feita em veiculos
		Root<Veiculo> root = criteria.from(Veiculo.class);
		
		Predicate [] predicates = criarRestricoes(veiculoFilter, builder, root);
		criteria.where(predicates);
		
		//Conta Quantos registros tem
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}
		
		
	
}

	

