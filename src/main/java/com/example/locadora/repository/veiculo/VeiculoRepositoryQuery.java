package com.example.locadora.repository.veiculo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.locadora.model.Veiculo;
import com.example.locadora.repository.filter.VeiculoFilter;

public interface VeiculoRepositoryQuery {
	
	public Page<Veiculo> filtrar(VeiculoFilter veiculoFilter, Pageable pageable);

}
