package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
