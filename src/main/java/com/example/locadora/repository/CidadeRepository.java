package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Cidade;


public interface CidadeRepository extends JpaRepository<Cidade, Long> {

}
