package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Pagamento;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

}
