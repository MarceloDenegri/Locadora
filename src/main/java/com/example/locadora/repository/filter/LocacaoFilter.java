package com.example.locadora.repository.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

public class LocacaoFilter {
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dtLocacaoDe;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dtLocacaoAte;
	
	private String pessoa;
	
	private String veiculo;
	
	private String id;
	
	

	public LocalDate getDtLocacaoDe() {
		return dtLocacaoDe;
	}

	public void setDtLocacaoDe(LocalDate dtLocacaoDe) {
		this.dtLocacaoDe = dtLocacaoDe;
	}

	public LocalDate getDtLocacaoAte() {
		return dtLocacaoAte;
	}

	public void setDtLocacaoAte(LocalDate dtLocacaoAte) {
		this.dtLocacaoAte = dtLocacaoAte;
	}

	public String getPessoa() {
		return pessoa;
	}

	public void setPessoa(String pessoa) {
		this.pessoa = pessoa;
	}

	public String getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(String veiculo) {
		this.veiculo = veiculo;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
