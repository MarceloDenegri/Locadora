package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Locacao;
import com.example.locadora.repository.locacao.LocacaoRepositoryQuery;

public interface LocacaoRepository extends JpaRepository<Locacao, Long>, LocacaoRepositoryQuery {

}
