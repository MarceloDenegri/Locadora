package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Marca;

public interface MarcaRepository extends JpaRepository<Marca, Long> {

}
