package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Combustivel;

public interface CombustivelRepository extends JpaRepository<Combustivel, Long>{

}
