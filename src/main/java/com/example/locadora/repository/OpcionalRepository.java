package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Opcional;

public interface OpcionalRepository extends JpaRepository<Opcional, Long>{

}
