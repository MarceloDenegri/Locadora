package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Modelo;

public interface ModeloRepository extends JpaRepository<Modelo, Long> {

}
