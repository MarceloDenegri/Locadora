package com.example.locadora.repository.locacao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.example.locadora.model.Locacao;
import com.example.locadora.model.Locacao_;
import com.example.locadora.model.Modelo_;
import com.example.locadora.model.Pessoa_;
import com.example.locadora.model.Veiculo_;
import com.example.locadora.repository.filter.LocacaoFilter;

public class LocacaoRepositoryImpl implements LocacaoRepositoryQuery {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<Locacao> filtrar(LocacaoFilter locacaoFilter, Pageable pageable){
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Locacao> criteria = builder.createQuery(Locacao.class);
		//root é usado para pegar os atributos da classe
		Root<Locacao> root = criteria.from(Locacao.class);
		//restriçoes filtros
		Predicate[] predicates = criarRestricoes(locacaoFilter, builder, root);
		criteria.where(predicates);
		
		
		TypedQuery<Locacao> query = manager.createQuery(criteria);
		
		//Adicionar na query a qtd total de resultados e onde ele vai começar
				adicionarRestircoesDePaginacao(query, pageable);
				
				//É passado o conteudo, o pageable o objeto e a quatidade de elementos para o filtro
			return new PageImpl<>(query.getResultList(), pageable, total(locacaoFilter));
		}

	

	private Predicate[] criarRestricoes(LocacaoFilter locacaoFilter, CriteriaBuilder builder, Root<Locacao> root) {
		
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(locacaoFilter.getPessoa())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Locacao_.pessoa).get(Pessoa_.nome)),
				"%" + locacaoFilter.getPessoa().toLowerCase()+ "%"));
		}
		
		if (!StringUtils.isEmpty(locacaoFilter.getVeiculo())) {
			predicates.add(builder.like(
		        	builder.lower(root.get(Locacao_.veiculo).get(Veiculo_.modelo).get(Modelo_.nome)),
				"%" + locacaoFilter.getVeiculo().toLowerCase()+ "%"));
		}
		
	
		
		if (locacaoFilter.getDtLocacaoDe() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Locacao_.dtretirda), locacaoFilter.getDtLocacaoDe()));
		}
		
		if (locacaoFilter.getDtLocacaoAte() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Locacao_.dtentrega), locacaoFilter.getDtLocacaoAte()));
		}
	
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private Long total(LocacaoFilter locacaoFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		//A consulta está sendo feita em locacao
		Root<Locacao> root = criteria.from(Locacao.class);
		
		Predicate [] predicates = criarRestricoes(locacaoFilter, builder, root);
		criteria.where(predicates);
		
		//Conta Quantos registros tem
		criteria.select(builder.count(root));
		
		return manager.createQuery(criteria).getSingleResult();
	}

	private void adicionarRestircoesDePaginacao(TypedQuery<Locacao> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalDeRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalDeRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalDeRegistrosPorPagina);
		
	}
	
}
