package com.example.locadora.repository.locacao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.example.locadora.model.Locacao;
import com.example.locadora.repository.filter.LocacaoFilter;

public interface LocacaoRepositoryQuery {

	public Page<Locacao> filtrar(LocacaoFilter locacaoFilter, Pageable pageable);
	
}
