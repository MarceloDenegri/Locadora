package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Motor;

public interface MotorRepository extends JpaRepository<Motor, Long> {

}
