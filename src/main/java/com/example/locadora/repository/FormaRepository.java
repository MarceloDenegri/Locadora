package com.example.locadora.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.locadora.model.Forma;

public interface FormaRepository extends JpaRepository<Forma, Long>{

}
