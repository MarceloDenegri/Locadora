/*package com.example.locadora;

import java.io.OutputStream;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.Connection;

import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class GerarRelatorios {
	private Connection conexao;

    public GerarRelatorios(Connection conexao) {
        this.conexao = conexao;
    }

    public void geraPdf(String jrxml, 
        Map<String, Object> parametros, OutputStream saida) {

        try {

            // compila jrxml em memoria
            JasperReport jasper = JasperCompileManager.compileReport(jrxml);

            // preenche relatorio
            JasperPrint print = JasperFillManager.fillReport(jasper, parametros, this.conexao);

            // exporta para pdf
            JRExporter exporter = new JRPdfExporter();
            exporter.setParameter(JRExporterParameter.JASPER_PRINT, print);
            exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, saida);

            exporter.exportReport();

        } catch (Exception e) {
            throw new RuntimeException("Erro ao gerar relatório", e);
        }
        
        HttpServletResponse response = // obtém o response do seu framework mvc
        		OutputStream saida = response.getOutpuStream();

        		GerarRelatorios gerador = new GerarRelatorios(conexao);
        		gerador.geraPdf("C:\Users\Usuário\Documents\workspace-sts-3.9.2.RELEASE\Locadora\src\MyReports\listagem.jrxml");
        
    }   
}*/