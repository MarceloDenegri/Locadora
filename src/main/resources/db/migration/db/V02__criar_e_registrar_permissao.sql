CREATE TABLE usuario (
	id BIGINT(20) PRIMARY KEY,
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	senha VARCHAR(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE permissao (
	id BIGINT(20) PRIMARY KEY,
	descricao VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE usuario_permissao (
	id_usuario BIGINT(20) NOT NULL,
	id_permissao BIGINT(20) NOT NULL,
	PRIMARY KEY (id_usuario, id_permissao),
	FOREIGN KEY (id_usuario) REFERENCES usuario(id),
	FOREIGN KEY (id_permissao) REFERENCES permissao(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO usuario (id, nome, email, senha) values (1, 'Administrador', 'admin@gmail.com', '$2a$10$.L2S721l48OYwuyINh/svecq3oJ9T57gctmhzvZk0894GvK6BZJv.');
INSERT INTO usuario (id, nome, email, senha) values (2, 'Maria Silva', 'maria@gmail.com', '$2a$10$HG1W6r9Im/8bIJM9IVDMKORzJ3l6a/EiMcIa.g.KrjHh5sbfzCAmO');

INSERT INTO permissao (id, descricao) values (1, 'ROLE_CADASTRAR_CATEGORIA');
INSERT INTO permissao (id, descricao) values (2, 'ROLE_REMOVER_CATEGORIA');
INSERT INTO permissao (id, descricao) values (3, 'ROLE_PESQUISAR_CATEGORIA');

INSERT INTO permissao (id, descricao) values (4, 'ROLE_CADASTRAR_PESSOA');
INSERT INTO permissao (id, descricao) values (5, 'ROLE_REMOVER_PESSOA');
INSERT INTO permissao (id, descricao) values (6, 'ROLE_PESQUISAR_PESSOA');

INSERT INTO permissao (id, descricao) values (7, 'ROLE_CADASTRAR_CIDADE');
INSERT INTO permissao (id, descricao) values (8, 'ROLE_REMOVER_CIDADE');
INSERT INTO permissao (id, descricao) values (9, 'ROLE_PESQUISAR_CIDADE');

INSERT INTO permissao (id, descricao) values (10, 'ROLE_CADASTRAR_COMBUSTIVEL');
INSERT INTO permissao (id, descricao) values (11, 'ROLE_REMOVER_COMBUSTIVEL');
INSERT INTO permissao (id, descricao) values (12, 'ROLE_PESQUISAR_COMBUSTIVEL');

INSERT INTO permissao (id, descricao) values (13, 'ROLE_CADASTRAR_ESTADO');
INSERT INTO permissao (id, descricao) values (14, 'ROLE_REMOVER_ESTADO');
INSERT INTO permissao (id, descricao) values (15, 'ROLE_PESQUISAR_ESTADO');

INSERT INTO permissao (id, descricao) values (16, 'ROLE_CADASTRAR_FORMA');
INSERT INTO permissao (id, descricao) values (17, 'ROLE_REMOVER_FORMA');
INSERT INTO permissao (id, descricao) values (18, 'ROLE_PESQUISAR_FORMA');

INSERT INTO permissao (id, descricao) values (19, 'ROLE_CADASTRAR_LOCACAO');
INSERT INTO permissao (id, descricao) values (20, 'ROLE_REMOVER_LOCACAO');
INSERT INTO permissao (id, descricao) values (21, 'ROLE_PESQUISAR_LOCACAO');

INSERT INTO permissao (id, descricao) values (22, 'ROLE_CADASTRAR_MARCA');
INSERT INTO permissao (id, descricao) values (23, 'ROLE_REMOVER_MARCA');
INSERT INTO permissao (id, descricao) values (24, 'ROLE_PESQUISAR_MARCA');

INSERT INTO permissao (id, descricao) values (25, 'ROLE_CADASTRAR_MODELO');
INSERT INTO permissao (id, descricao) values (26, 'ROLE_REMOVER_MODELO');
INSERT INTO permissao (id, descricao) values (27, 'ROLE_PESQUISAR_MODELO');

INSERT INTO permissao (id, descricao) values (28, 'ROLE_CADASTRAR_MOTOR');
INSERT INTO permissao (id, descricao) values (29, 'ROLE_REMOVER_MOTOR');
INSERT INTO permissao (id, descricao) values (30, 'ROLE_PESQUISAR_MOTOR');

INSERT INTO permissao (id, descricao) values (31, 'ROLE_CADASTRAR_OPCIONAL');
INSERT INTO permissao (id, descricao) values (32, 'ROLE_REMOVER_OPCIONAL');
INSERT INTO permissao (id, descricao) values (33, 'ROLE_PESQUISAR_OPCIONAL');

INSERT INTO permissao (id, descricao) values (34, 'ROLE_CADASTRAR_PAGAMENTO');
INSERT INTO permissao (id, descricao) values (35, 'ROLE_REMOVER_PAGAMENTO');
INSERT INTO permissao (id, descricao) values (36, 'ROLE_PESQUISAR_PAGAMENTO');

INSERT INTO permissao (id, descricao) values (37, 'ROLE_CADASTRAR_VEICULO');
INSERT INTO permissao (id, descricao) values (38, 'ROLE_REMOVER_VEICULO');
INSERT INTO permissao (id, descricao) values (39, 'ROLE_PESQUISAR_VEICULO');

-- admin
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 1);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 2);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 3);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 4);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 5);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 6);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 7);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 8);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 9);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 10);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 11);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 12);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 13);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 14);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 15);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 16);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 17);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 18);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 19);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 20);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 21);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 22);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 23);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 24);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 25);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 26);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 27);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 28);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 29);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 30);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 31);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 32);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 33);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 34);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 35);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 36);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 37);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 38);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (1, 39);

-- maria
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (2, 2);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (2, 5);
INSERT INTO usuario_permissao (id_usuario, id_permissao) values (2, 9);