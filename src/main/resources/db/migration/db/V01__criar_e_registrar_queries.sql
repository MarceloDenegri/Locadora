create table estado(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50) NOT NULL,
	uf CHAR(2)
)Engine=innodb DEFAULT Charset=utf8;

insert into locadora.estado (nome,uf) values ('Rio grande do Sul', 'RS');
insert into locadora.estado (nome,uf) values ('Santa Catarina', 'SC');
insert into locadora.estado (nome,uf) values ('Rio de Janeiro', 'RJ');
insert into locadora.estado (nome,uf) values ('Parana', 'SC');


CREATE TABLE cidade (
	id INT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	id_estado INT(20) NOT NULL,
	FOREIGN KEY (id_estado) REFERENCES locadora.estado(id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
INSERT INTO cidade (nome, id_estado) values ('Serafina Correa', 1);
INSERT INTO cidade (nome, id_estado) values ('Florianopolis', 2);
INSERT INTO cidade (nome, id_estado) values ('Rio de Janeiro', 3);
INSERT INTO cidade (nome, id_estado) values ('Guapore', 1);
INSERT INTO cidade (nome, id_estado) values ('Casca', 1);
INSERT INTO cidade (nome, id_estado) values ('Passo Fundo', 1);
INSERT INTO cidade (nome, id_estado) values ('Blumenau', 2);
INSERT INTO cidade (nome, id_estado) values ('Morro do salgueiro', 3);
INSERT INTO cidade (nome, id_estado) values ('Curitiba', 4);

create table combustivel(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50) NOT NULL
)Engine=innodb DEFAULT Charset=utf8;

insert into combustivel (nome) values ('Alcool');
insert into combustivel (nome) values ('Diesel');
insert into combustivel (nome) values ('Gasolina');

create table categoria(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50) NOT NULL
)Engine=innodb DEFAULT Charset=utf8;

insert into categoria (nome) values ('Luxo');
insert into categoria (nome) values ('Esporte');
insert into categoria (nome) values ('Popular');

create table motor(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	potencia VARCHAR(50) NOT NULL
)Engine=innodb DEFAULT Charset=utf8;

insert into motor (potencia) values ('4.0');
insert into motor (potencia) values ('2.0');
insert into motor (potencia) values ('1.0');

create table marca(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50) NOT NULL
)Engine=innodb DEFAULT Charset=utf8;

insert into marca (nome) values ('Ferrari');
insert into marca (nome) values ('BMW');
insert into marca (nome) values ('Fiat');

CREATE TABLE modelo (
	id INT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	id_marca INT(20) NOT NULL,
	FOREIGN KEY (id_marca) REFERENCES marca(id)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	
INSERT INTO modelo (nome, id_marca) values ('Ferrari 200', 1);
INSERT INTO modelo (nome, id_marca) values ('BMW 206', 2);
INSERT INTO modelo (nome, id_marca) values ('Palio', 3);

create table opcional(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	nome VARCHAR(50) NOT NULL,
	qtd Int(20) NOT NULL
)Engine=innodb DEFAULT Charset=utf8;

insert into opcional (nome, qtd) values ('GPS','1');
insert into opcional (nome, qtd) values ('Bebe conforto','4');
insert into opcional (nome, qtd) values ('Assento de elevação','5');

create table pessoa(
	id INT(20) AUTO_INCREMENT PRIMARY KEY,
	tipo Varchar (50) NOT NULL,
	nome VARCHAR(50) NOT NULL,
	email Varchar(50)Not NULL,
	telefone Varchar(50) NOT NULL,
	id_cidade int(20) not null,
	cpf varchar(50),
	cnpj varchar(50),
	FOREIGN KEY (id_cidade) REFERENCES locadora.cidade(id)
)Engine=innodb DEFAULT Charset=utf8;

insert into pessoa (tipo, nome,   email, telefone, id_cidade) values ('Fisica', 'Arthur',  'email', '3444-1640', 1);
insert into pessoa (tipo, nome,   email, telefone, id_cidade) values ('Juridica', 'BarDenegri',   'email', '3444-1640', 2);

create table veiculo(
id int(20) AUTO_INCREMENT PRIMARY KEY,
placa varchar(50) NOT NULL,
vidro boolean,
trava boolean,
ar boolean,
cambio boolean,
portas int(5),
valor float(20),
disponivel boolean,
id_combustivel int(20) not null,
id_categoria int(20) not null,
id_modelo int(20) not null,
id_motor int(20) not null,
id_opcional int(20) not null,
id_marca int(20) not null,
FOREIGN KEY (id_combustivel) REFERENCES locadora.combustivel(id),
FOREIGN KEY (id_categoria) REFERENCES locadora.categoria(id),
FOREIGN KEY (id_modelo) REFERENCES locadora.modelo(id),
FOREIGN KEY (id_motor) REFERENCES locadora.motor(id),
FOREIGN KEY (id_opcional) REFERENCES locadora.opcional(id),
FOREIGN KEY (id_marca) REFERENCES locadora.marca(id)
)Engine=innodb DEFAULT Charset=utf8;

insert into veiculo (placa, vidro, trava, ar, cambio, portas, valor, disponivel,  id_combustivel, id_categoria, id_modelo, id_motor, id_opcional, id_marca) values ('ghg4455',true, true, true, true, '5', '100', true,1, 1, 1, 1, 1, 1);
insert into veiculo (placa, vidro, trava, ar, cambio, portas, valor, disponivel,  id_combustivel, id_categoria, id_modelo, id_motor, id_opcional, id_marca) values ('ghg6666',true, false, true, true, '4', '110', true,2, 2, 2, 2, 3, 2);
insert into veiculo (placa, vidro, trava, ar, cambio, portas, valor, disponivel,  id_combustivel, id_categoria, id_modelo, id_motor, id_opcional, id_marca) values ('ghg3333',true, false, true, true, '2', '110', true,1, 1, 2, 2, 1, 2);

create table pagamento(
id int(20) AUTO_INCREMENT primary key,
valor_recebido double(7,2),
dt_pagamento date
)Engine=innodb DEFAULT Charset=utf8;

insert into pagamento(valor_recebido, dt_pagamento) values ('122', '2018/01/01'); 


create table forma(
id int(20) AUTO_INCREMENT PRIMARY KEY,
nome varchar(50) not null
)Engine=innodb DEFAULT Charset=utf8;

insert into forma(nome) values ('cartao');
insert into forma(nome) values ('dinheiro');
insert into forma(nome) values ('cheque');

create table Locacao(
id int(20) AUTO_INCREMENT PRIMARY KEY,
dt_entrega date not null,
dt_retirada date not null,
valor_total float(20),
saldo float,
id_pessoa int(20) NOT NULL,
id_veiculo int(20) NOT NULL,
id_pagamento int (20) not null,
id_forma int (20) not null,
FOREIGN KEY (id_pessoa) REFERENCES locadora.pessoa(id),
FOREIGN KEY (id_veiculo) REFERENCES locadora.veiculo(id),
FOREIGN KEY (id_pagamento) REFERENCES locadora.pagamento(id),
FOREIGN KEY (id_forma) REFERENCES forma(id)
)Engine=innodb DEFAULT Charset=utf8;

insert into locacao (dt_entrega, dt_retirada, valor_total, saldo, id_pessoa, id_veiculo, id_pagamento, id_forma) values ('2018/02/02', '2018/01/02/', '100', '10', 1, 1, 1,3);





